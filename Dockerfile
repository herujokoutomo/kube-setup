# syntax=docker/dockerfile:1
FROM golang:1.17 AS builder
WORKDIR /go/app
COPY main.go    ./
COPY go.* ./
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -mod=readonly -v -o app .

FROM alpine:latest  
WORKDIR /root/
COPY --from=builder /go/app/app ./
EXPOSE 3000
CMD ["./app"]  