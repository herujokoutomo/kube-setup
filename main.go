package main

import (
	"net/http"
	"github.com/labstack/echo/v4"
)

func main() {
	type Health struct {
		Version string `json:"version"`
		Name string `json:"name"`
	}

	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		healthcheck := Health{
			Name: "Hello Kube Gitlab",
			Version: "0.1.0",
		}
		return c.JSON(http.StatusOK, healthcheck)
	})
	e.Logger.Fatal(e.Start(":3000"))
}